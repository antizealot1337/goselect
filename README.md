# goselect

**NOTE:** This is _**alpha**_ qualit software. It works but doesn't handle
irregularies very well. It's core functionality is usable however. No
guarantees are made about the command line API or how this software
downloads, extracts, or manager Go toolchains in general. You have been
warned.

A utility to help install and use different version of the Go programming
language.

## Usage

The general usage is to use `goselect <version>` where _<version>_ is the
version of Go you want to use. If the version isn't downloaded it will be.
After the toolchain is downloaded goselect builds itself as a "proxy" command
meaining it will dispatch commands to the go, godoc, and gofmt commands
respectively.

Along with using a particular version you can also use commands to list the
versions and even remove previously downloaded toolchains.

## NOTE

The toolchains directory has been moved from `$HOME/.gotoolchains` to 
`$HOME/.goselect/toolchains`. Soon it will be used to keep track of the current
toochains selected and allow for proxies to be used to the executables in the
toolchains directories.

## License
Licenced under the terms of the MIT license. View the LICENSE file for more
information.
