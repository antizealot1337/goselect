package gosemver

import (
	"errors"
	"testing"
)

func TestParse(t *testing.T) {
	t.Run("Bad", func(t *testing.T) {
		inputs := []string{
			"",
			"bad",
			"1",
		}

		for _, input := range inputs {
			_, err := Parse(input)

			if err == nil {
				t.Errorf(`expected error for "%s"`, input)
			} //if
		} //for
	}) //func

	t.Run("Good", func(t *testing.T) {
		good := []struct {
			input   string
			version Version
		}{
			{
				input: "1.17",
				version: Version{
					Major:    1,
					Minor:    17,
					Patch:    -1,
					Modifier: "",
				},
			},
			{
				input: "1.17.5",
				version: Version{
					Major:    1,
					Minor:    17,
					Patch:    5,
					Modifier: "",
				},
			},
			{
				input: "1.18beta2",
				version: Version{
					Major:    1,
					Minor:    18,
					Patch:    -1,
					Modifier: "beta2",
				},
			},
		}

		for _, value := range good {
			v, err := Parse(value.input)
			if err != nil {
				t.Errorf(`unexpected error for (%s): %v`, value.input, err)
				continue
			} //if

			if expected, actual := value.version.Major, v.Major; actual != expected {
				t.Errorf(`expected major %d but was %d for "%s"`, expected, actual, value.input)
			} //if

			if expected, actual := value.version.Minor, v.Minor; actual != expected {
				t.Errorf(`expected minor %d but was %d for "%s"`, expected, actual, value.input)
			} //if

			if expected, actual := value.version.Patch, v.Patch; actual != expected {
				t.Errorf(`expected patch %d but was %d for "%s"`, expected, actual, value.input)
			} //if

			if expected, actual := value.version.Modifier, v.Modifier; actual != expected {
				t.Errorf(`expected modifier "%s" but was "%s" for "%s"`, expected, actual, value.input)
			} //if
		} //for
	}) //func
} //func

func TestMustParse(t *testing.T) {
	t.Run("Fail", func(t *testing.T) {
		defer func() {
			val := recover()

			err, ok := val.(error)
			if !ok {
				t.Error("expected error to be thrown")
				return
			} //if

			if !errors.Is(err, ErrBadVersionString) {
				t.Error("expected ErrInvalidVersion", err)
			} //if
		}() //func

		MustParse("")
	}) //func
	t.Run("Good", func(t *testing.T) {
		v := MustParse("1.17.6")

		if expected, actual := uint64(1), v.Major; actual != expected {
			t.Errorf("expected major %d but was %d", expected, actual)
		} //if

		if expected, actual := uint64(17), v.Minor; actual != expected {
			t.Errorf("expected minor %d but was %d", expected, actual)
		} //if

		if expected, actual := int64(6), v.Patch; actual != expected {
			t.Errorf("expected patch %d but was %d for ", expected, actual)
		} //if
	}) //func
} //func

func TestVersionLess(t *testing.T) {
	oneFour := Version{
		Major: 1,
		Minor: 4,
		Patch: 3,
	}

	if oneFour.Less(&oneFour) {
		t.Error("a version should not be less than itself")
	} //if

	oneSixteen := Version{
		Major: 1,
		Minor: 16,
		Patch: -1,
	}

	if !oneFour.Less(&oneSixteen) {
		t.Errorf("%s should be less than %s", oneFour, oneSixteen)
	} //if

	if oneSixteen.Less(&oneFour) {
		t.Errorf("%s should not be less than %s", oneSixteen, oneFour)
	} //if

	oneSixteenBeta := Version{
		Major:    1,
		Minor:    16,
		Patch:    -1,
		Modifier: "beta1",
	}

	if !oneFour.Less(&oneSixteenBeta) {
		t.Errorf("%s should be less than %s", oneFour, oneSixteenBeta)
	} //if

	if oneSixteenBeta.Less(&oneFour) {
		t.Errorf("%s should not be less than %s", oneSixteenBeta, oneFour)
	} //if

	if !oneSixteenBeta.Less(&oneSixteen) {
		t.Errorf("%s should be less than %s", oneSixteenBeta, oneSixteen)
	} //if
} //func

func Test_hasPatch(t *testing.T) {
	v := Version{
		Patch: -1,
	}

	if v.hasPatch() {
		t.Error("expected not has patch")
	} //if

	v.Patch = 0

	if !v.hasPatch() {
		t.Error("expected has patch")
	} //if

	v.Patch = 1

	if !v.hasPatch() {
		t.Error("expected has patch")
	} //if
} //func

func Test_hasModifier(t *testing.T) {
	var v Version

	if v.hasModifier() {
		t.Error("expected not has modifier")
	} //if

	v.Modifier = "a"

	if !v.hasModifier() {
		t.Error("expected has modifier")
	} //if
} //func

func Test_isValid(t *testing.T) {
	t.Run("Invalid", func(t *testing.T) {
		v := Version{
			Patch:    1,
			Modifier: "a",
		}

		if v.isValid() {
			t.Error("expected invalid")
		} //if
	}) //func
	t.Run("Valid", func(t *testing.T) {
		vs := []Version{
			{
				Patch: 1,
			},
			{
				Patch:    -1,
				Modifier: "a",
			},
		}

		for _, v := range vs {
			if !v.isValid() {
				t.Errorf("expected valid patch=%d modifier=%s", v.Patch, v.Modifier)
			} //if
		} //for
	}) //func
} //func

func TestVersionString(t *testing.T) {
	data := []struct {
		out     string
		version Version
	}{
		{
			out: "1.17",
			version: Version{
				Major: 1,
				Minor: 17,
				Patch: -1,
			},
		},
		{
			out: "1.17.6",
			version: Version{
				Major: 1,
				Minor: 17,
				Patch: 6,
			},
		},
		{
			out: "1.18beta2",
			version: Version{
				Major:    1,
				Minor:    18,
				Patch:    -1,
				Modifier: "beta2",
			},
		},
	}

	for _, d := range data {
		if expected, actual := d.out, d.version.String(); actual != expected {
			t.Errorf(`expected output "%s" but was "%s"`, expected, actual)
		} //if
	} //for
} //func
