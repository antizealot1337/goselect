package gosemver

import (
	"errors"
	"fmt"
	"regexp"
	"strconv"
)

var versionRE = regexp.MustCompile(`(\d+)\.(\d+)\.?(\d+)?(.+)?`)

// ErrBadVersionString meaning the version string is invalid
var ErrBadVersionString = errors.New("invalid version string")

// ErrInvalidVersion is an error returned when the Version object is not valid.
var ErrInvalidVersion = errors.New("invalid version")

// Version represents semantic versions of the go packaging. The Go archives
// don't strictly follow the standard. For example the first version of 1.15 for
// Linux is named g1.15.linux-amd64.tar.gz. To be compliant the package name
// would be go1.15.0.linux-amd64.tar.gz.
type Version struct {
	Major    uint64
	Minor    uint64
	Patch    int64
	Modifier string
} //struct

// Parse a Go style semver from a string.
func Parse(s string) (*Version, error) {
	matches := versionRE.FindAllStringSubmatch(s, 1)

	if len(matches) == 0 {
		return nil, ErrBadVersionString
	} //if

	match := matches[0]

	var v Version
	var err error

	v.Major, err = strconv.ParseUint(match[1], 10, 64)
	if err != nil {
		return nil, ErrBadVersionString
	} //if

	v.Minor, err = strconv.ParseUint(match[2], 10, 64)
	if err != nil {
		return nil, ErrBadVersionString
	} //if

	if len(match) == 2 {
		return &v, nil
	} //if

	if len(match[3]) != 0 {
		v.Patch, err = strconv.ParseInt(match[3], 10, 64)
		if err != nil {
			return nil, ErrBadVersionString
		} //if
	} else {
		v.Patch = -1
	} //if

	if len(match) == 3 {
		return &v, nil
	} //if

	v.Modifier = match[4]

	return &v, nil
} //func

// MustParse calls Parse and will panic is there is an error parsing the Version
// from the string.
func MustParse(s string) *Version {
	v, err := Parse(s)
	if err != nil {
		panic(err)
	} //func
	return v
} //func

// Less determines if this Version is less than the other Version.
func (v Version) Less(o *Version) bool {
	switch {
	case v.Major < o.Major:
		return true
	case v.Major > o.Major:
		return false
	} //switch

	switch {
	case v.Minor < o.Minor:
		return true
	case v.Minor > o.Minor:
		return false
	} //switch

	switch {
	case v.hasModifier() && o.hasModifier():
		return v.Modifier < o.Modifier
	case v.hasModifier() && !o.hasModifier():
		return true
	case !v.hasModifier() && o.hasModifier():
		return false
	} //switch

	return v.normalizedPatch() < o.normalizedPatch()
} //func

// hasPatch returns true if the Version has a patch. A Version is considered to
// have a patch if Patch is greater than or equal to zero.
func (v Version) hasPatch() bool {
	return v.Patch >= 0
} //func

// hasModifier return true if the Version has a modifier. A Version is
// considered to have a modifier if Modifer is not an empty string.
func (v Version) hasModifier() bool {
	return v.Modifier != ""
} //func

// isValid returns true if the Version has no patch or modifier or a patch or
// modifier but not both.
func (v Version) isValid() bool {
	return !(v.hasModifier() && v.hasPatch())
} //func

// normalizedPath return the patch value of a Version. If the version doesn't
// have a patch it will return a zero.
func (v Version) normalizedPatch() int64 {
	if !v.hasPatch() {
		return 0
	} //if

	return v.Patch
} //func

func (v Version) String() string {
	if !v.isValid() {
		panic(ErrInvalidVersion)
	} //if

	switch {
	case v.hasPatch():
		return fmt.Sprintf("%d.%d.%d", v.Major, v.Minor, v.Patch)
	case v.hasModifier():
		return fmt.Sprintf("%d.%d%s", v.Major, v.Minor, v.Modifier)
	default:
		return fmt.Sprintf("%d.%d", v.Major, v.Minor)
	} //switch
} //func
