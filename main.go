package main // import "bitbucket.org/antizealot1337/goselect"

import (
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"sort"

	semver "bitbucket.org/antizealot1337/goselect/gosemver"
)

const (
	usage = `Usage:
	goselect <version>                  Select a version (download if necessary).
	goselect cur|current|default        Show the current toolchain version.
	goselect ls|list                    List the downloaded versions.
	goselect rm|remove <versions...>    Remove a downloaded version.
	goselect dl|download <versions...>  Remove a downloaded version.
	goselect build-proxies              Build new proxy commands.
	goselect help|--help                Show the help
`
)

var (
	home       string
	selectHome string
	toolchains string
	quiet      bool
	source     = "https://go.dev/dl/"
	proxies    = []string{
		"go",
		"godoc",
		"gofmt",
	}
)

func init() {
	// Set the home directory
	home = os.Getenv(homeEnv)

	// Make sure home exists
	if home == "" {
		panic(fmt.Errorf("error: %s not set", homeEnv))
	} //if

	// Set the select home
	selectHome = filepath.Join(home, goselectDir)

	// Set the toolchain home
	toolchains = filepath.Join(selectHome, "toolchains")

} //func

func main() {
	// Loop through the proxy commands
	for _, proxycmd := range proxies {
		// Get the base command
		basecmd := filepath.Base(os.Args[0])

		// Check if goselect is being used as a proxy
		if proxycmd == basecmd {
			// Proxy
			proxy(basecmd, os.Args)

			// Done
			return
		} //if
	} //for

	// Setup the command line flags
	flag.StringVar(&toolchains, "location", toolchains,
		"Location to install Go toolchins")
	flag.StringVar(&source, "url", source,
		"Web server with Go toolchian archives")
	flag.BoolVar(&quiet, "quiet", quiet, "Disable output")

	// Create the usage function
	flag.Usage = func() {
		fmt.Print(usage)
		fmt.Println()
		fmt.Println("Options:")
		flag.PrintDefaults()
		fmt.Println()
		fmt.Println(`Use "go help <command>" for command specific help`)
	} //func

	// Parse the command line flags
	flag.Parse()

	// Set the args
	args := flag.Args()

	// Make sure there are enough command line arguments
	if len(args) == 0 {
		flag.Usage()
		os.Exit(2)
	} //if

	var err error

	// Determine what to do
	switch args[0] {
	case "current", "default", "cur":
		var cur string
		cur, err = readCurrent()

		// Check for an error
		if err != nil {
			break
		} //if

		fmt.Println(cur)
	case "ls", "list":
		// Make sure toolchains exists
		err = ensureExists(toolchains)

		// Check for an error
		if err != nil {
			break
		} //if

		// Open the toolchains directory
		var tcdir *os.File
		tcdir, err = os.Open(toolchains)

		// Check for an error
		if err != nil {
			break
		} //if

		// Close later
		defer tcdir.Close()

		// Read the directory names
		var dirs []string
		dirs, err = tcdir.Readdirnames(-1)

		// Check for an error
		if err != nil {
			break
		} //if

		// Sort the slice
		sort.Slice(dirs, func(i, j int) bool {
			di := semver.MustParse(dirs[i][2:])
			dj := semver.MustParse(dirs[j][2:])
			return di.Less(dj)
		})

		// List the directories
		for _, dir := range dirs {
			fmt.Println(dir[2:])
		} //for
	case "rm", "remove":
		if len(args) < 2 {
			err = fmt.Errorf("error: no version provided")
			break
		} //if

		// Make sure the tools directory exists
		err = ensureExists(toolchains)

		// Check for an error
		if err != nil {
			break
		} //if

		// Loop through the version strings
		for _, vstr := range args[1:] {
			// Remove the version
			err = os.RemoveAll(filepath.Join(toolchains, "go"+vstr))

			// Check for an error
			if err != nil && !quiet {
				// Print the error but don't stop
				fmt.Println("error:", err)
			} //if
		} //for

		// Check for an error
		if err != nil {
			err = fmt.Errorf("error: failed to remove some versions")
			break
		} //if
	case "dl", "download":
		if len(args) < 2 {
			err = fmt.Errorf("error: no version provided")
			break
		} //if

		// Make sure the tools directory exists
		err = ensureExists(toolchains)

		// Check for an error
		if err != nil {
			break
		} //if

		// Loop through possible version
		for _, vstr := range args[1:] {
			// Download the version
			_, err = download(vstr)

			// Check for an error
			if err != nil {
				break
			} //if
		} //for
	case "build-proxies":
		// Read the current version
		var cur string
		cur, err = readCurrent()

		// Check for an error
		if err != nil {
			break
		} //if

		// Build the proxies
		err = buildProxies(cur)
	case "help":
		flag.Usage()
	default:
		// Make sure the toolchains directory exists
		err = ensureExists(toolchains)

		// Check for an error
		if err != nil {
			break
		} //if

		// Set the version
		var version *semver.Version
		version, err = semver.Parse(args[0])

		if err != nil {
			flag.Usage()
			break
		} //if

		vstr := version.String()

		// Check if the version exists
		if !exists(filepath.Join(toolchains, "go"+vstr)) {
			// Download the version
			var archive string
			archive, err = download(vstr)

			// Check for an error
			if err != nil {
				break
			} //if

			// Extract the archive
			err = extract(archive, vstr)

			// Check for an error
			if err != nil {
				break
			} //if

			// Remove the archive
			// os.Remove(archive)
		} //if

		// Set the current go version
		err = set(vstr)
	} //switch

	// Check for an error
	if err != nil {
		fmt.Fprintln(os.Stderr, err.Error())
		os.Exit(1)
	} //if
} //func

func ensureExists(path string) error {
	// Check if the path already exists with a stat
	_, err := os.Stat(path)

	// Check if an error was found
	if err != nil {
		// Check if the error is a "not exist" error
		if os.IsNotExist(err) {
			// Create the directory
			return os.MkdirAll(path, 0755)
		} //if

		return err
	} //if

	return nil
} //func

func exists(path string) bool {
	// Attempt to stat the path
	_, err := os.Stat(path)

	// Assume all errors indicate the path doesn't exist
	return err == nil
} //func

func download(version string) (archive string, err error) {
	// Determine the archive name
	archname := archiveName(version)

	// Build the URL
	url, err := buildURL(source, archname)

	if err != nil {
		return "", err
	} //if

	if !quiet {
		fmt.Println("Downloading", url)
	} //if

	// Where to download the toolchains
	downloads := filepath.Join(selectHome, "downloads")

	// Create the archive path
	archive = filepath.Join(downloads, archname)

	// Check if the archive was already downloaded
	if exists(archive) {
		if !quiet {
			fmt.Println("Archive already downloaded")
			fmt.Println("Skipping download")
		} //if

		return
	} //if

	// Make a request
	resp, err := http.Get(url)

	// Check for an error
	if err != nil {
		return "", err
	} //if

	// Check the response code
	if resp.StatusCode != http.StatusOK {
		return "", fmt.Errorf("error: bad response(%d) %s", resp.StatusCode,
			resp.Status)
	} //if

	// Close the body later
	defer resp.Body.Close()

	// Ensure the download folder exists
	if !exists(downloads) {
		// Create a directory
		if err = os.Mkdir(downloads, 0700); err != nil {
			return "", fmt.Errorf("error: no download file: %v", err)
		} //if
	} //if

	// Create file in the temp directory for downloading
	out, err := os.Create(archive + ".part")

	// Check for an error
	if err != nil {
		return "", err
	} //if

	// Copy the body to the archive
	_, err = io.Copy(out, resp.Body)

	// Check if an error occurred
	if err != nil {
		return "", fmt.Errorf("error: download failed: %v", err)
	} //if

	// Rename the archive
	err = os.Rename(archive+".part", archive)

	return
} //func

func buildURL(base, archive string) (string, error) {
	u, err := url.Parse(base)
	if err != nil {
		return "", err
	} //if

	u.Path = path.Join(u.Path, archive)

	return u.String(), nil
} //func

func set(version string) error {
	// Write the current version file
	err := ioutil.WriteFile(
		filepath.Join(selectHome, "current"),
		[]byte(version),
		0644,
	)

	// Check for an error
	if err != nil {
		return fmt.Errorf("set write current error: %v", err)
	} //if

	// Check if goselect is in the path
	_, err = exec.LookPath("goselect")

	// Check for an error
	if err != nil {
		// The path to the go binary
		gobin := filepath.Join(toolchains, "go"+version, "bin", "go")

		// goselect path
		gosel := "bitbucket.org/antizealot1337/goselect"

		// Install goselect
		err = exec.Command(gobin, "install", "-u", gosel).Run()

		// Check for an error
		if err != nil {
			return err
		} //if

		// Build the proxies
		err = buildProxies(version)

		// Check for an error
		if err != nil {
			return err
		} //if
	} //if

	// Deprecated: The following is only for backwards compatibility

	// Remove any existing symlink
	err = os.Remove(filepath.Join(home, ".go"))

	// Check for an error
	if err != nil && !os.IsNotExist(err) {
		return fmt.Errorf("remove old link error: %v", err)
	} //if

	// Create a new symlink
	return os.Symlink(
		filepath.Join(toolchains, "go"+version),
		filepath.Join(home, ".go"),
	)
} //func

func buildProxies(version string) (err error) {
	// The path to the go binary
	gobin := filepath.Join(toolchains, "go"+version, "bin", "go")

	// goselect path
	gosel := "bitbucket.org/antizealot1337/goselect"

	// Loop through the slice of proxy commands to build
	for _, proxy := range proxies {
		// Build the proxy
		err = exec.Command(gobin, "build", "-o", proxy, gosel).Run()

		// Check for an error
		if err != nil {
			return fmt.Errorf("build proxy error: %v", err)
		} //if

		// Install the proxy
		err = install(proxy)

		// Check for an error
		if err != nil {
			return fmt.Errorf("install proxy error: %v", err)
		} //if
	} //for

	return nil
} //func

func install(bin string) error {
	return os.Rename(bin, filepath.Join(findBinPath(), bin))
} //func

func findBinPath() string {
	var pth string
	if pth = os.Getenv("GOBIN"); pth != "" {
		return pth
	} //if
	if pth = os.Getenv("GOPATH"); pth != "" {
		return filepath.Join(pth, "bin")
	} //if
	return filepath.Join(home, "go", "bin")
} //func

func readCurrent() (string, error) {
	// Read the current toolchain version
	verb, err := ioutil.ReadFile(filepath.Join(selectHome, "current"))

	// Check for an error
	if err != nil {
		return "", err
	} //if

	return string(verb), nil
} //func

func proxy(gcmd string, args []string) {
	// Read the current toolchain version
	verb, err := ioutil.ReadFile(filepath.Join(selectHome, "current"))

	// Check for an error
	if err != nil {
		panic(fmt.Errorf("read version error: %v", err))
	} //if

	// Set the version
	ver := string(verb)

	// Create the command
	cmd := exec.Command(filepath.Join(toolchains, "go"+ver, "bin", gcmd), args[1:]...)

	// Set the input, output, and error streams
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	// Execute the program and check for an error
	cmd.Run()
} //func
