// +build !windows

package main

import (
	"archive/tar"
	"compress/gzip"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"runtime"
)

func archiveName(version string) string {
	return fmt.Sprintf("go%s.%s-%s.tar.gz", version, runtime.GOOS,
		runtime.GOARCH)
} //func

func extract(archive, version string) error {
	if !quiet {
		fmt.Println("Extracting archive")
	} //if

	// Open the archive file
	in, err := os.Open(archive)

	// Check for an error
	if err != nil {
		return err
	} //if

	// Close the file later
	defer in.Close()

	// Create a GZIP reader
	gzread, err := gzip.NewReader(in)

	// Check for an error
	if err != nil {
		return err
	} //if

	// Create a TAR reader
	tread := tar.NewReader(gzread)

	// The TAR header
	thead, err := tread.Next()

	// Loop
	for {
		// Check if the end of the TAR file has been reached
		if err == io.EOF {
			break
		} //if

		outpath := filepath.Clean(thead.Name[2:])

		// Determine what to do based on the type
		switch thead.Typeflag {
		case tar.TypeReg:
			// Open a file to which to copy
			//out, err := os.Create(filepath.Join(toolchains, "go"+version, outpath))
			out, err := os.OpenFile(
				filepath.Join(toolchains, "go"+version, outpath),
				os.O_WRONLY|os.O_CREATE,
				os.FileMode(thead.Mode),
			)

			// Check for an error
			if err != nil {
				return err
			} //if

			_, err = io.Copy(out, tread)

			// Check for an error
			if err != nil {
				return err
			} //if
		case tar.TypeDir:
			// Create a directory
			if err := os.MkdirAll(filepath.Join(toolchains, "go"+version, outpath), 0755); err != nil {
				return err
			} //if
		default:
			// Ignore
		} //switch

		// Get the next file
		thead, err = tread.Next()
	} //for

	return nil
} //func
