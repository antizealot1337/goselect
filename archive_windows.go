// +build windows

package main

import (
	"archive/zip"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"runtime"
)

func archiveName(version string) string {
	return fmt.Sprintf("go%s.windows-%s.zip", version, runtime.GOARCH)
} //func

func extract(archive, version string) error {
	if !quiet {
		fmt.Println("Extracting archive")
	} //if

	// Open the archive file
	in, err := zip.OpenReader(archive)

	// Check for an error
	if err != nil {
		return err
	} //if

	// Close later
	defer in.Close()

	// Loop through the files
	for _, file := range in.File {
		// Get the info
		info := file.FileHeader.FileInfo()

		outpath := filepath.Clean(file.Name[2:])

		// Determine if the file is a directory or no
		if info.IsDir() {
			// Create a directory
			if err := os.MkdirAll(filepath.Join(toolchains, "go"+version, outpath), 0755); err != nil {
				return err
			} //if
		} else {
			// Open the file for reading
			zin, err := file.Open()

			// Check for an error
			if err != nil {
				return err
			} //if

			// Close the file later
			defer zin.Close()

			// Create a file
			out, err := os.OpenFile(
				filepath.Join(toolchains, "go"+version, outpath),
				os.O_WRONLY|os.O_CREATE,
				os.FileMode(info.Mode()),
			)

			// Check for an error
			if err != nil {
				return err
			} //if

			// Close the file later
			defer out.Close()

			// Copy to the new file
			_, err = io.Copy(out, zin)

			if err != nil {
				return err
			} //if
		} //if
	} //for

	return nil
} //func
